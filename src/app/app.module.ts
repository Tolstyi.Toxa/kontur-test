import {NgModule} from "@angular/core";
import {HttpModule} from "@angular/http";
import {BrowserModule} from "@angular/platform-browser";

import {ComponentModule} from "./../component/component.module";

import {AppComponent} from "./app/app.component";

import {KladrService} from "./services/kladr/kladr.service";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        ComponentModule,
        HttpModule
    ],
    providers: [
        KladrService
    ]
})
export class AppModule {
}
