import {OptionItem} from "../../../component/autocomplete/types";

export interface IKladrItem {
    Id: number;
    City: string;
}

export class KladrOptionItem extends OptionItem {
}
