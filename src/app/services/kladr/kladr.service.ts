import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {IOptionDataSource, IOptionSearchResults, OptionItem} from "../../../component/autocomplete/types";
import {InvalidDataException, InvalidHttpException} from "../exceptions";
import {IKladrItem, KladrOptionItem} from "./kladr.types";

@Injectable()
export class KladrService implements IOptionDataSource {
    private kladr: Array<IKladrItem>;
    constructor(private http: Http) {
        this.kladr = require("./kladr.json");
     }

    public getById(id: string): Promise<OptionItem> {
        return Promise
            .resolve(this.kladr)
            .catch((err) => {
                throw new InvalidHttpException();
            })
            .then(() => {
                const _id: number = parseInt(id, 10);
                const items: Array<IKladrItem> = this
                    .kladr
                    .filter((x: IKladrItem) => x.Id === _id);

                if (items.length !== 1) {
                    throw new InvalidDataException();
                }

                return items
                    .map((x: IKladrItem) => new KladrOptionItem({
                        id: x.Id.toString(),
                        text: x.City
                    }))
                    [0];
            });
    }

    public getByText(text: string): Promise<OptionItem> {
        return Promise
            .resolve(this.kladr)
            .catch((err) => {
                throw new InvalidHttpException();
            })
            .then(() => {
                return new Promise((resolve, reject) => {
                    window.setTimeout(() => {
                        const _text: string = (text || "").trim().toLowerCase();
                        const items: Array<IKladrItem> = this
                            .kladr
                            .filter((x: IKladrItem) => (x.City || "").toLowerCase()  === _text);

                        if (items.length !== 1) {
                            reject(new InvalidDataException());
                        }

                        return resolve(items
                            .map((x: IKladrItem) => new KladrOptionItem({
                                id: x.Id.toString(),
                                text: x.City
                            }))
                            [0]);
                    }, 300);
                });
            })
            .then((x: OptionItem) => x);
    }

    public searchBy(text: string, all: boolean = false): Promise<IOptionSearchResults> {
        return Promise
            .resolve(this.kladr)
            .catch((err) => {
                throw new InvalidHttpException();
            })
            .then(() => {
                return new Promise((resolve, reject) => {
                    window.setTimeout(() => {
                        const _text: string = (text || "").trim().toLowerCase();
                        const items: Array<IKladrItem> = this
                            .kladr
                            .filter((x: IKladrItem) => (x.City || "").toLowerCase().startsWith(_text));

                        const filteredItems: Array<OptionItem> = items
                            .map((x: IKladrItem) => new KladrOptionItem({
                                id: x.Id.toString(),
                                text: x.City
                            }));

                        if (all) {
                            resolve({
                                count: filteredItems.length,
                                values: filteredItems
                            });

                            return;
                        }

                        resolve({
                            count: filteredItems.length,
                            values: (filteredItems.length > 100)
                                ? filteredItems.slice(0, 20)
                                : (
                                    (filteredItems.length > 50)
                                        ? filteredItems.slice(0, 10)
                                        : filteredItems.slice(0, 5)
                                )
                        });
                    }, 1200);
                });
            })
            .then((x: IOptionSearchResults) => x);
    }
}
