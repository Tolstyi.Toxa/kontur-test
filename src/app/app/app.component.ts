import {Component} from "@angular/core";

import {KladrService} from "./../services/kladr/kladr.service";

import {IOptionDataSource} from "../../component/autocomplete/types";

@Component({
    selector: "as-app",
    styleUrls: ["./app.component.styl"],
    templateUrl: "./app.component.html"
})
export class AppComponent {
    public kladrService: IOptionDataSource;
    constructor(kladrService: KladrService) {
        this.kladrService = kladrService;
    }
}
