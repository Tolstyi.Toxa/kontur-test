import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
    ViewChild
} from "@angular/core";
import {IAutocompleteComponent} from "./autocomplete";
import {InvalidDataException, InvalidHttpException} from "../../app/services/exceptions";
import {LoaderController} from "./LoaderController";
import {IPopupComponent} from "./../popup/popup";
import {
    ArrowStyleEnum,
    AutocompleteComponentCode,
    ComponentSizeEnum,
    IComponentMessages,
    IOptionDataSource,
    IOptionSearchResults,
    Messages,
    OptionItem,
    ValidateResultEnum,
    ValidateRuleResultEnum
} from "./types";

const KEYBOARD_DELAY_MS: number = 300;

@Component({
    selector: "as-autocomplete",
    styleUrls: ["./autocomplete.component.styl"],
    templateUrl: "./autocomplete.component.html"
})
export class AutocompleteComponent implements AfterViewInit, OnInit, OnDestroy, IAutocompleteComponent {
    @Input("arrow-style")
    public arrowStyle: ArrowStyleEnum = ArrowStyleEnum.NoArrow;
    @Input("caption")
    public caption: string = "";
    @Input("datasource")
    public set datasource(value: IOptionDataSource) {
        if (this._datasource !== value) {
            this._datasource = value;
            this.loadValue();
        }
    }
    public get datasource(): IOptionDataSource {
        return this._datasource;
    }
    @Input("enabled")
    public enabled: boolean = true;
    public focus: boolean = false;
    public hover: boolean = false;
    @ViewChild("input")
    public input: ElementRef;
    @ViewChild("inputOnwer")
    public inputOnwer: ElementRef;
    public isLoaderShow: boolean = false;
    @Input("placeholder")
    public placeholder: string = "Начните вводить";
    @Input("popup")
    public popup: IPopupComponent;
    @Input("required")
    public set required(value: boolean) {
        if (value !== this._required) {
            this._required = value;
            this.revalidate();
        }
    }
    public get required(): boolean {
        return this._required;
    }

    @Input("selectedId")
    public set selectedId(value: string) {
        if (this._selectedId !== value) {
            this._selectedId = value;
            this
                .loadValue();
        }
    }

    @Input("selectedText")
    public set selectedText(value: string) {
        if (this._text !== value) {
            this._selectedId = null;
            this._text = value;
            this
                .loadValue();
        }
    }

    @Input("size")
    public set size(value: ComponentSizeEnum) {
        this._size = value;
        this.changeSize();
    }
    public get size(): ComponentSizeEnum {
        return this._size;
    }
    @Input("tabindex")
    public tabindex: number = 1;
    public set text(value: string) {
        if (value !== this._text) {
            this._text = value;
            this._selectedId = null;
            this.beginShow();
        }
    }
    // Отображающаяся строка поиска. Используется только в шаблоне
    public get text(): string {
        return this._text;
    }
    // Исключительно для использования в шаблоне, для проверки следует использовать validate()
    public get validateResult(): ValidateResultEnum {
        return this._validateResult;
    }

    // Исключительно для использования в шаблоне
    public get validateText(): string {
        return this._validateText;
    }

    // А вдруг мы прямо сча пересчитываем что-нить?
    public get value(): Promise<OptionItem> {
        if (!this._valueStackSize) {
            return Promise.resolve(this._value);
        }

        return this
            ._valuePromise
            .then(() => {
                return this.value;
            });
    }
    @Output("valueChanged")
    public valueChanged: EventEmitter<OptionItem> = new EventEmitter<OptionItem>();

    protected get code(): number {
        return this._code;
    }
    protected get messages(): IComponentMessages {
        return Messages[this.code];
    }

    private _code: number = AutocompleteComponentCode;
    private _datasource: IOptionDataSource;
    private _isInited: boolean = false;
    private _required: boolean = false;
    private _selectedId: string = "";
    private _size: ComponentSizeEnum = ComponentSizeEnum.Normal;

    private _showPopupStackSize: number = 0;
    private _showPopupCode: number = 0;
    private _showPopupPromise: Promise<void>;

    private _text: string;
    private _validateResult: ValidateResultEnum;
    private _validateText: string;

    private _value: OptionItem;
    private _valuePromise: Promise<void>;
    private _valueCode: number = 0;
    private _valueStackSize: number = 0;

    private _loaderController: LoaderController;
    private _updateTimer: number;

    constructor(private elementRef: ElementRef) {
        this.setValidate(ValidateResultEnum.NoCheck, this.messages["VLD-NO-CHECK"]);
        this._loaderController = new LoaderController();
        this._loaderController.show.subscribe(() => {
            this.isLoaderShow = true;
        });
        this._loaderController.hide.subscribe(() => {
            this.hideLoader();
        });
    }

    public ngAfterViewInit(): void {
        this.changeSize();
    }

    public ngOnDestroy(): void {
        this._loaderController.clearAll();
        this._loaderController.show.unsubscribe();
        this._loaderController.hide.unsubscribe();
        this.clearUpdateTimer();
    }

    public ngOnInit(): void {
        this._isInited = true;
        this.loadValue();
    }

    public onInputBlur(event: Event): void {
        this.focus = false;
        this.hidePopup();
        if (
            this.validateResult === ValidateResultEnum.InCheck
        ) {
            this._loadValue();
        }
    }

    public onInputClick(event: Event): void {
        this.selectAll();
    }

    public onInputFocus(event: Event): void {
        this.focus = true;
        if (this.arrowStyle === ArrowStyleEnum.Arrow) {
            if (this._value) {
                this.showPopup("", this._value.text());
            } else {
                this.showPopup();
            }
        } else {
            if (!this._value) {
                this.showPopup();
            }
        }
    }

    public onInputBlockMouseEnter(event: Event): void {
        this.hover = true;
    }

    public onInputBlockMouseLeave(event: Event): void {
        this.hover = false;
    }

    public onKeyDownArrowUp(event: KeyboardEvent): void {
        this.popup.focusUp(this);
        event.preventDefault();
    }

    public onKeyDownArrowDown(event: KeyboardEvent): void {
        this.popup.focusDown(this);
        event.preventDefault();
    }

    public onKeyDownEnter(event: KeyboardEvent): void {
        if (this.popup) {
            const item: OptionItem = this.popup.focusedItem(this) || null;
            if (item) {
                this._setValueDirty(item);
                this.focusNextTabbableComponent();
            } else if (this.popup.canReload(this)) {
                this.popup.hideReload(this);
                this.showPopup();
            }
        }
    }

    public onKeyDownEscape(event: KeyboardEvent): void {
       this.popup.hide();
    }

    // Возвращаем значение для проверки в форме. Promise - мало ли, что как и с какой скоростью мы наменяли
    public validate(): Promise<ValidateResultEnum> {
        if (this._valueStackSize) {
            return this
                ._valuePromise
                .then(() => {
                    return this._validateResult;
                });
        }

        return Promise
            .resolve(this._validateResult);
    }

    public setValueDirty(value: OptionItem): void {
        this._setValueDirty(value);
    }

    public reloadPopup(): void {
        this.showPopup();
    }

    private focusNextTabbableComponent(): void {
        const tabbables: NodeListOf<Element> = document
            .querySelectorAll("a:not([disabled]), button:not([disabled]), input[type=text]:not([disabled]), [tabindex]:not([disabled]):not([tabindex='-1'])");
        // get all tabable elements
        const source: HTMLElement = this.input.nativeElement as HTMLElement;
        for (let i: number = 0, n: number = tabbables.length; i < n; ++i) {
             // loop through each element
            if (tabbables[i] === source) {
                if (i + 1 < n) {
                    const tabbable: HTMLInputElement = tabbables[i + 1] as HTMLInputElement;
                    tabbable.focus();
                    tabbable.setSelectionRange(0, tabbable.value.length);
                    break;
                }
            }
        }
    }

    private selectAll(): void {
        (this.input.nativeElement as HTMLInputElement)
            .setSelectionRange(
                0,
                (this.input.nativeElement as HTMLInputElement).value.length
            );
    }

    private _setValueDirty(value: OptionItem): void {
        this.terminateLoadValue();
        this.terminateShowPopup();
        this.clearUpdateTimer();
        this._loaderController.clearShow();
        this._loaderController.clearHide();
        this.isLoaderShow = false;
        this.hidePopup();
        this._setValue(
            value,
            value.id(),
            value.text(),
            () => {
                this._validate(value);
            }
        );
    }

    private revalidate(): void {
        if (!this._isInited) {
            return;
        }

        if (!this._valueStackSize) {
            this._validate(this._value);
        }
    }

    private initUpdateTimer(): void {
        this.clearUpdateTimer();
        this.terminateShowPopup();

        this._updateTimer = window.setTimeout(
            () => {
                this.showPopup();
                this._updateTimer = null;
            },
            KEYBOARD_DELAY_MS
        );
    }

    private clearUpdateTimer(): void {
        if (this._updateTimer) {
            window.clearTimeout(this._updateTimer);
        }
        this._updateTimer = null;
    }

    private getPopupItems(text: string): Promise<IOptionSearchResults> {
        return this
            ._datasource
            .searchBy(text, this.arrowStyle === ArrowStyleEnum.Arrow)
            .then((data: IOptionSearchResults) => {
                return data;
            })
            .catch((err) => {
                if (err instanceof InvalidHttpException) {
                    return null;
                }

                throw err;
            });
    }

    private terminateShowPopup(): void {
        if (this._showPopupStackSize) {
            ++ this._showPopupStackSize;

            this._showPopupPromise = this
                ._showPopupPromise
                .then(() => {
                    --this._showPopupStackSize;
                });
        }
    }

    private showPopup(searchText: string = this._text, selectedText: string = null): Promise<void> {
        if (!this._datasource) {
            return Promise.resolve();
        }

        ++ this._showPopupStackSize;
        ++ this._showPopupCode;

        this._loaderController.initShow();

        let promise: Promise<IOptionSearchResults>;
        if (this._showPopupStackSize === 1) {
            promise = this.getPopupItems(searchText);
        } else {
            promise = this._showPopupPromise
                .then(() => {
                    return this.getPopupItems(searchText);
                });
        }

        this._showPopupPromise = promise
            .then((data: IOptionSearchResults) => {
                --this._showPopupStackSize;
                if (!this.hasLoading()) {
                    if (this.focus && this.popup) {
                        this.popup.show(
                            this,
                            data,
                            selectedText || searchText
                        );
                    }

                    this._loaderController.clearShow();
                    this.hideLoader();
                }
            });
        return this._showPopupPromise;
    }

    private hidePopup(): void {
        if (this.popup) {
            this.popup.hide();
        }
    }

    private hasLoading(): boolean {
        return this._showPopupStackSize > 0 || this._valueStackSize > 0;
    }

    private hideLoader(): void {
        if (!(this.hasLoading()) && this._loaderController.canHide) {
            this.isLoaderShow = false;
        }
    }

    // Задержка с тем, чтобы не мучать сервер
    private beginShow(): void {
        if (this._value) {
            this._value = null;
        }
        this.setValidateInCheck();
        this.initUpdateTimer();
    }

    private _setValue(value: OptionItem, id: string, text: string, beforeEmit: () => void): void {
        this._value = value;
        if (this._value) {
            this._selectedId = value.id();
            this._text = value.text();
        } else {
            if (id) {
                this._text = null;
            }
        }
        beforeEmit();
        this.valueChanged.emit(value);
    }

    private setValuePromise(code: number, id: string, text: string): Promise<void> {
        let p: Promise<OptionItem>;

        if (!this.datasource) {
            p = Promise
                .resolve(null);
        } else {
            const _id: string = (id || "").toString().trim();
            if (_id) {
                p = this
                    .datasource
                    .getById(_id);
            } else if (text && text.trim()) {
                p = this
                    .datasource
                    .getByText(text);
            } else {
                p = Promise
                    .resolve(null);
            }
        }

        return p
            .then((value: OptionItem) => {
                if (code === this._valueCode) {
                    this
                        ._setValue(
                            value,
                            id,
                            text,
                            () => {
                                this._validate(value);
                            }
                        );
                }
            })
            .catch((err) => {
                if (err instanceof InvalidDataException) {
                    this._setValue(
                        null,
                        id,
                        text,
                        () => {
                            this.setValidate(ValidateResultEnum.Error, this.messages["VLD-INVALID-VALUE"]);
                        }
                    );
                } else if (err instanceof InvalidHttpException) {
                    this._setValue(
                        null,
                        id,
                        text,
                        () => {
                            this.setValidate(ValidateResultEnum.Error, this.messages["ERR-GET-DATA"]);
                        }
                    );
                } else {
                    throw err;
                }
            });
    }

    private _loadValue(): Promise<void> {
        ++ this._valueStackSize;
        ++ this._valueCode;

        this._loaderController.initShow();
        if (this._value) {
            this._value = null;
        }

        this.setValidateInCheck();
        let promise: Promise<void>;
        if (this._valueStackSize === 1) {
            promise = this.setValuePromise(this._valueCode, this._selectedId, this._text);
        } else {
            promise = this
                ._valuePromise
                .then(() => {
                    return this.setValuePromise(this._valueCode, this._selectedId, this._text);
                });

        }

        this._valuePromise = promise
            .then(() => {
                --this._valueStackSize;
                if (!this.hasLoading()) {
                    this._loaderController.clearShow();
                    this.hideLoader();
                }
            });

        return this._valuePromise;
    }

    private terminateLoadValue(): void {
        if (this._valueStackSize) {
            ++ this._valueStackSize;

            this._valuePromise = this
                ._valuePromise
                .then(() => {
                    --this._valueStackSize;
                });
        }
    }

    private loadValue(): Promise<void> {
        if (!this._isInited) {
            return Promise.resolve();
        }

        return this._loadValue();
    }

    private changeSize(): void {
        const classList: DOMTokenList = (this.elementRef.nativeElement as HTMLElement)
            .classList;

        classList.remove("component-size-big", "component-size-normal", "component-size-small");

        switch (this.size) {
            case ComponentSizeEnum.Big:
                classList.add("component-size-big");
                break;
            case ComponentSizeEnum.Normal:
                classList.add("component-size-normal");
                break;
            case ComponentSizeEnum.Small:
                classList.add("component-size-small");
                break;
        }
    }

    private setValidate(result: ValidateResultEnum, text: string): void {
        this._validateResult = result;
        this._validateText = text;
    }

    private setValidateInCheck(): void {
        if (this._validateResult !== ValidateResultEnum.InCheck) {
            this.setValidate(ValidateResultEnum.InCheck, this.messages["VLD-IN-CHECK"]);
        }
    }

    private ValidateRequired(value: OptionItem): ValidateRuleResultEnum {
        if (!this.required) {
            return ValidateRuleResultEnum.NoCheck;
        }

        if (value) {
            return ValidateRuleResultEnum.Ok;
        }

        return ValidateRuleResultEnum.Fail;
    }

    private _validate(value: OptionItem): void {
        if (!this.datasource) {
            if (this.ValidateRequired(value) === ValidateRuleResultEnum.Fail) {
                if (!this._text) {
                    this.setValidate(ValidateResultEnum.Error, this.messages["VLD-NO-DS-REQUIRED-EMPTY"]);
                } else {
                    this.setValidate(ValidateResultEnum.Error, this.messages["VLD-NO-DS"]);
                }
            } else {
                this.setValidate(ValidateResultEnum.Error, this.messages["VLD-NO-DS"]);
            }
        } else if (this.ValidateRequired(value) === ValidateRuleResultEnum.Fail) {
            if (!this._text) {
                this.setValidate(ValidateResultEnum.Error, this.messages["VLD-REQ"]);
            } else {
                this.setValidate(ValidateResultEnum.Error, this.messages["VLD-INVALID-VALUE"]);
            }
        } else if (!value && this._text) {
            this.setValidate(ValidateResultEnum.Error, this.messages["VLD-INVALID-VALUE"]);
        } else {
            this.setValidate(ValidateResultEnum.Ok, "");
        }
    }
}
