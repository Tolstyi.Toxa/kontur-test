import {ElementRef} from "@angular/core";
import {ArrowStyleEnum, OptionItem} from "./types";

export interface IAutocompleteComponent {
    inputOnwer: ElementRef;
    arrowStyle: ArrowStyleEnum;
    setValueDirty(value: OptionItem): void;
    reloadPopup(): void;
}
