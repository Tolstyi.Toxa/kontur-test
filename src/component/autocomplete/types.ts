export enum ArrowStyleEnum {
    NoArrow = 0,
    Arrow = 1
}

export enum ComponentSizeEnum {
    Small = 0,
    Normal = 1,
    Big = 2
}

export enum ValidateRuleResultEnum {
    NoCheck = -1,
    Ok = 0,
    Fail = 1
}

export enum ValidateResultEnum {
    // Проверка еще не выполнялась, проверка не возможна
    NoCheck = -2,
    // Прямо сейчас выполняется проверка значения
    InCheck = -1,
    // Проверка успешно выполнена
    Ok = 0,
    // Есть предупреждения
    Warning = 1,
    // Проверка провалена
    Error = 2
}

export const AutocompleteComponentCode: number = 1;

export const ValidateTextInCheckDefault: string = "Выполняется проверка значения";
export const ValidateTextNoCheckDefault: string = "Проверка значения еще не выполнялась";

export const Messages: IMessages = {
    [AutocompleteComponentCode]: {
        "VLD-IN-CHECK": ValidateTextInCheckDefault,
        "VLD-NO-CHECK": ValidateTextNoCheckDefault,
        "VLD-NO-DS": "Источник данных не задан, проверка значений не возможна",
        "VLD-NO-DS-REQUIRED-EMPTY": "Источник данных не задан, проверка значений не возможна. Обязательное поле не заполненно",
        "VLD-REQ": "Обязательное поле не заполненно",
        "VLD-INVALID-VALUE": "Выберите значение из списка",

        "ERR-GET-DATA": "Что-то пошло не так. Проверьте соединение с интернетом и попробуйте еще раз"
    }
};

export interface IOptionItem {
    id: string;
    text: string;
}

export abstract class OptionItem {
    private _id: string;
    private _text: string;

    constructor(source: IOptionItem) {
        this._id = source.id;
        this._text = source.text;
    }

    public id(): string {
        return this._id;
    }

    public text(): string {
        return this._text;
    }
}

export interface IMessages {
    [componentCode: number]: IComponentMessages;
}

export interface IComponentMessages {
    [messageCode: string]: string;
}

export interface IOptionSearchResults {
    count: number;
    values: Array<OptionItem>;
}

export interface IOptionDataSource {
    getById(id: string): Promise<OptionItem>;
    getByText(text: string): Promise<OptionItem>;
    searchBy(text: string, all?: boolean): Promise<IOptionSearchResults>;
}
