import {EventEmitter} from "@angular/core";

const LOADER_DELAY_SHOW_MS: number = 500;
const LOADER_DELAY_HIDE_MS: number = 1000;

export class LoaderController {
    public show: EventEmitter<void> = new EventEmitter<void>(false);
    public hide: EventEmitter<void> = new EventEmitter<void>(false);
    public get canHide(): boolean {
        return this._canHide;
    }

    private _hideTimer: number;
    private _showTimer: number;
    private _canHide: boolean = false;

    public clearShow(): void {
        if (this._showTimer) {
            window.clearTimeout(this._showTimer);
        }
        this._showTimer = null;
    }

    public initShow(): void {
        this.clearShow();

        if (!this._hideTimer) {
            this._canHide = true;
        }

        this._showTimer = window.setTimeout(
            () => {
                this._showTimer = null;
                this.initHide();
                this.show.emit();
            },
            LOADER_DELAY_SHOW_MS
        );
    }

    public clearHide(): void {
        if (this._hideTimer) {
            window.clearTimeout(this._hideTimer);
        }
        this._hideTimer = null;
    }

    public clearAll(): void {
        this.clearShow();
        this.clearHide();
    }

    private initHide(): void {
        this.clearHide();
        this._canHide = false;

        this._hideTimer = window.setTimeout(
            () => {
                this._canHide = true;
                this._hideTimer = null;
                this.hide.emit();
            },
            LOADER_DELAY_HIDE_MS
        );
    }
}
