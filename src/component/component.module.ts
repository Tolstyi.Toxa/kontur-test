import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";

import {AutocompleteComponent} from "./autocomplete/autocomplete.component";
import {PopupComponent} from "./popup/popup.component";

@NgModule({
    declarations: [
        AutocompleteComponent,
        PopupComponent
    ],
    exports: [
        AutocompleteComponent,
        PopupComponent
    ],
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule
    ]
})
export class ComponentModule {
}
