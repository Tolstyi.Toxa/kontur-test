export enum ValidateStateEnum {
    Ok = 0,
    Warning = 1,
    Error = 2
}

export interface IValidate {
    state: ValidateStateEnum;
    text: string;
}

export interface IErrorDescription {
    text: string;
    validate?: IValidate;
};

export const Errors: {[key: string]: IErrorDescription} = {
    "ERR-C-00001": {
        text: "Что-то пошло не так. Проверьте соединение с интернетом и попробуйте еще раз",
        validate: {
            state: ValidateStateEnum.Error,
            text: "Что-то пошло не так. Проверьте соединение с интернетом и попробуйте еще раз"
        }
    }
};