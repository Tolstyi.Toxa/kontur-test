import {Component, ElementRef, HostListener, Input, OnDestroy, ViewChild} from "@angular/core";
import {IAutocompleteComponent} from "../autocomplete/autocomplete";
import {
    ArrowStyleEnum,
    IOptionSearchResults,
    OptionItem
} from "../autocomplete/types";
import {IPopupComponent, PopupItemsOveflowTextFunc} from "./popup";

interface IOffset {
    left: number;
    top: number;
}

const MAX_HEIGHT: number = 450;
const MIN_HEIGHT: number = 200;

@Component({
    selector: "as-popup",
    styleUrls: ["./popup.component.styl"],
    templateUrl: "./popup.component.html"
})
export class PopupComponent implements IPopupComponent, OnDestroy {
    public selectedIndex: number = -1;
    public focusedIndex: number = -1;
    public showReload: boolean = false;
    @ViewChild("content")
    public content: ElementRef;
    public set isShow(value: boolean) {
        if (this.elementRef) {
            const el: HTMLElement = this.elementRef.nativeElement as HTMLElement;
            if (value) {
                el.classList.add("show-popup");
            } else {
                el.classList.remove("show-popup");
            }

        }
        this._isShow = value;
    }
    public set left(value: number) {
        if (this.elementRef) {
            const el: HTMLElement = this.elementRef.nativeElement as HTMLElement;
            if (typeof value !== "undefined") {
                el.style.left = value + "px";
            } else {
                el.style.left = null;
            }

        }
        this._left = value;
    }
    public set top(value: number) {
        if (this.elementRef) {
            const el: HTMLElement = this.elementRef.nativeElement as HTMLElement;
            if (typeof value !== "undefined") {
                el.style.top = value + "px";
            } else {
                el.style.top = null;
            }

        }
        this._top = value;
    }
    public set bottom(value: number) {
        if (this.elementRef) {
            const el: HTMLElement = this.elementRef.nativeElement as HTMLElement;
            if (typeof value !== "undefined") {
                el.style.bottom = value + "px";
            } else {
                el.style.bottom = null;
            }

        }
        this._bottom = value;
    }
    public set height(value: number) {
        if (this.content) {
            const el: HTMLElement = this.content.nativeElement as HTMLElement;
            if (typeof value !== "undefined") {
                el.style.height = value + "px";
            } else {
                el.style.height = null;
            }

        }
        this._height = value;
    }
    public set arrowStyle(value: ArrowStyleEnum) {
        if (this.elementRef) {
            const el: HTMLElement = this.elementRef.nativeElement as HTMLElement;
            if (value === ArrowStyleEnum.Arrow) {
                el.classList.add("popup-arrowed");
            } else {
                el.classList.remove("popup-arrowed");
            }

        }
        this._arrowStyle = value;
    }
    public get arrowStyle(): ArrowStyleEnum {
        return this._arrowStyle;
    }
    public result: IOptionSearchResults;
    public overflowTextFunc: PopupItemsOveflowTextFunc = this.defaultOverflowText;

    private _autocomplete: IAutocompleteComponent;
    private _redrawTimer: number;
    private _showFocusedTimer: number;
    private _source: ElementRef;
    private _left: number;
    private _top: number;
    private _bottom: number;
    private _height: number;
    private _isShow: boolean;
    private _arrowStyle: ArrowStyleEnum;

    constructor(private elementRef: ElementRef) { }

    public ngOnDestroy(): void {
        this.clearRedraw();
        this.clearShowFocusedTimer();
    }

    public show(
        autocomplete: IAutocompleteComponent,
        result: IOptionSearchResults,
        text: string
    ): void {
        this._autocomplete = autocomplete;
        this._source = autocomplete.inputOnwer;
        this.arrowStyle = autocomplete.arrowStyle;

        this.showReload = !result;
        this.result = result;
        this.left = -10000;
        this.top = -10000;
        this.bottom = undefined;
        this.height = undefined;
        this.selectedIndex = -1;
        this.focusedIndex = -1;
        if (result && result.values.length) {
            result.values.forEach((x: OptionItem, i: number) => {
                if (this.selectedIndex === -1) {
                    if (x.text() === text) {
                        this.selectedIndex = i;
                    }
                }
            });
            this.focusedIndex = (this.selectedIndex === -1) ? 0 : this.selectedIndex;
        }
        this.isShow = true;
        if (this.content) {
            (this.content.nativeElement as HTMLElement).scrollTop = 0;
        }
        this.initRedraw();
    }

    public hide(): void {
        this.isShow = false;
        this._autocomplete = null;
        this.clearShowFocusedTimer();
        this.clearRedraw();
    }

    public focusUp(autocomplete: IAutocompleteComponent): void {
        if (
            this._isShow &&
            this._autocomplete && this._autocomplete === autocomplete &&
            this.result && this.result.values.length
        ) {
            if (this.focusedIndex === 0) {
                this.focusedIndex = this.result.values.length - 1;
                this.initShowFocusedTimer(false);
            } else {
                --this.focusedIndex;
                this.initShowFocusedTimer(true);
            }
        }
    }

    public focusDown(autocomplete: IAutocompleteComponent): void {
        if (
            this._isShow &&
            this._autocomplete && this._autocomplete === autocomplete &&
            this.result && this.result.values.length
        ) {
            if (this.focusedIndex === this.result.values.length - 1) {
                this.focusedIndex = 0;
                this.initShowFocusedTimer(true);
            } else {
                this.focusedIndex++;
                this.initShowFocusedTimer(false);
            }
        }
    }

    public onPopupClick(event: Event): boolean {
        event.preventDefault();
        event.stopImmediatePropagation();
        event.stopPropagation();
        return false;
    }

    public onPopupContentMouseWheel(event: MouseWheelEvent): boolean {
        if (this.content) {
            let scrollTo: number = null;

            if (event.type === "mousewheel") {
                scrollTo = (event.wheelDeltaY * -1);
            }

            if (scrollTo) {
                event.preventDefault();
                (this.content.nativeElement as HTMLElement).scrollTop += scrollTo;
            }
        }

        return true;
    }

    public onReloadClick(event: Event): void {
        this._hideReload();

        this._autocomplete.reloadPopup();
    }

    public canReload(autocomplete: IAutocompleteComponent): boolean {
        if (
            this._autocomplete && this._autocomplete === autocomplete &&
            this.showReload
        ) {
            return true;
        }

        return false;
    }

    public hideReload(autocomplete: IAutocompleteComponent): void {
        if (this._autocomplete && this._autocomplete === autocomplete) {
            this._hideReload();
        }
    }

    public onItemClick(event: Event, index: number): void {
        if (this._autocomplete && this._isShow) {
            this._autocomplete.setValueDirty(this.result.values[index]);
        }
    }

    public focusedItem(autocomplete: IAutocompleteComponent): OptionItem {
        if (
            this._isShow &&
            this._autocomplete && this._autocomplete === autocomplete &&
            this.result && this.result.values.length
        ) {
            return this.result.values[this.focusedIndex];
        }
        return null;
    }

    @HostListener("mousedown", ["$event"])
    private onMouseDown(event: MouseEvent): boolean {
        event.preventDefault();
        return false;
    }

    private _hideReload(): void {
        this.showReload = false;
    }

    private defaultOverflowText(result: IOptionSearchResults): string {
        if (!result) {
            return ``;
        }

        return `Показано ${result.values.length} из ${result.count} найденных значений. Уточните запрос, чтобы увидеть остальные строки`;
    }

    private getOffsetRect(elem: HTMLElement): IOffset {
        // (1)
        const box: ClientRect = elem.getBoundingClientRect();

        // (2)
        const body: HTMLElement = document.body;
        const docElem: HTMLElement = document.documentElement;

        // (3)
        const scrollTop: number = window.pageYOffset || docElem.scrollTop || body.scrollTop;
        const scrollLeft: number = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;

        // (4)
        const clientTop: number = docElem.clientTop || body.clientTop || 0;
        const clientLeft: number = docElem.clientLeft || body.clientLeft || 0;

        // (5)
        const top: number  = box.top +  scrollTop - clientTop;
        const left: number = box.left + scrollLeft - clientLeft;

        return {top, left};
    }

    private setHeight(thisHeight: number, baseHeight: number): void {
        if (this.content) {
            const contentHtml: HTMLElement = (this.content || this.elementRef).nativeElement as HTMLElement;
            const deltaHeight: number = thisHeight - contentHtml.offsetHeight;
            this.height = Math.min(contentHtml.offsetHeight, baseHeight - deltaHeight);
        }
    }

    private redraw(): void {
        const parentHtml: HTMLElement = (this.elementRef.nativeElement as HTMLElement)
            .parentElement;
        const thisHtml: HTMLElement = this.elementRef.nativeElement as HTMLElement;
        const sourceHtml: HTMLElement = this._source.nativeElement as HTMLElement;

        const parentOffset: IOffset = this.getOffsetRect(parentHtml);
        const sourceOffset: IOffset = this.getOffsetRect(sourceHtml);

        const offsetTop: number = sourceOffset.top - parentOffset.top;
        const offsetLeft: number = sourceOffset.left - parentOffset.left;
        const offsetBottom: number =
            (parentOffset.top + parentHtml.offsetHeight) - (sourceOffset.top + sourceHtml.offsetHeight);

        this.showFocusedTop();
        // Синтетический сдвиг, учитываем border, margin
        this.left = offsetLeft - 2 - 3;

        if (offsetBottom > Math.min(MIN_HEIGHT, thisHtml.offsetHeight)) {
            // Можем поместить попап снизу
            // Синтетический сдвиг
            this.top = (offsetTop + sourceHtml.offsetHeight) + 2;
            this.bottom = undefined;
            this.setHeight(thisHtml.clientHeight, offsetBottom);
            return;
        }

        if (offsetTop > Math.min(MIN_HEIGHT, thisHtml.offsetHeight)) {
            // Можем поместить попап сверху
            this.top = undefined;
            // Синтетический сдвиг
            this.bottom = (offsetBottom + sourceHtml.offsetHeight) + 2;
            this.setHeight(thisHtml.clientHeight, offsetTop);
            return;
        }

        // Накрайняк пихаем куда придется и как придется, c прижимом по нижнему краю
        this.top = undefined;
        this.bottom = offsetBottom + sourceHtml.offsetHeight;
        this.setHeight(thisHtml.clientHeight, parentHtml.offsetHeight);
    }

    private initShowFocusedTimer(up: boolean = false): void {
        this.clearShowFocusedTimer();

        this._showFocusedTimer = window.setTimeout(() => {
            if (up) {
                this.showFocusedTop();
            } else {
                this.showFocusedBottom();
            }
            this._showFocusedTimer = null;
        }, 5);
    }

    private clearShowFocusedTimer(): void {
        if (this._showFocusedTimer) {
            window.clearTimeout(this._showFocusedTimer);
            this._showFocusedTimer = null;
        }
    }

    private showFocusedTop(): void {
        if (this.content) {
            const el: HTMLElement = (this.content.nativeElement as HTMLElement);
            const sel: NodeListOf<Element> = el.getElementsByClassName("focused");

            if (sel.length === 1) {
                const item: HTMLElement = sel[0] as HTMLElement;
                if (item.offsetTop < el.scrollTop || item.offsetTop > el.scrollTop + el.clientHeight) {
                    el.scrollTop = item.offsetTop;
                }
            }
        }
    }

    private showFocusedBottom(): void {
        if (this.content) {
            const el: HTMLElement = (this.content.nativeElement as HTMLElement);
            const sel: NodeListOf<Element> = el.getElementsByClassName("focused");

            if (sel.length === 1) {
                const item: HTMLElement = sel[0] as HTMLElement;
                const bottom: number = item.offsetTop + item.offsetHeight;
                if (bottom > el.clientHeight + el.scrollTop || bottom < el.scrollTop) {
                    el.scrollTop = bottom - el.clientHeight;
                }
            }
        }
    }

    private initRedraw(): void {
        this.clearRedraw();
        this._redrawTimer = window
            .setTimeout(() => {
                this.clearShowFocusedTimer();
                this.redraw();
                this._redrawTimer = null;
            },
            5
        );
    }

    private clearRedraw(): void {
        if (!this._redrawTimer) {
            window.clearInterval(this._redrawTimer);
        }

        this._redrawTimer = null;
    }
}
