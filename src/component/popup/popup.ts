import {IAutocompleteComponent} from "../autocomplete/autocomplete";
import {IOptionSearchResults, OptionItem} from "../autocomplete/types";

export type PopupItemsOveflowTextFunc = (result: IOptionSearchResults) => string;

export interface IPopupComponent {
    show(
        autocomplete: IAutocompleteComponent,
        result: IOptionSearchResults,
        text: string
    ): void;
    hide(): void;
    focusUp(autocomplete: IAutocompleteComponent): void;
    focusDown(autocomplete: IAutocompleteComponent): void;
    focusedItem(autocomplete: IAutocompleteComponent): OptionItem;
    canReload(autocomplete: IAutocompleteComponent): boolean;
    hideReload(autocomplete: IAutocompleteComponent): void;
}
