const commonConfig = require("./webpack.common");
const webpackMerge = require("webpack-merge");
const helpers = require("./helpers");
const ENV = process.env.ENV = process.env.NODE_ENV = "development";

module.exports = webpackMerge(
    commonConfig({
        env: ENV
    }),
    {
        module: {
            rules: [
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader'],
                    include: [
                        helpers.root('src', 'styles')
                    ]
                },
                {
                    test: /\.styl$/,
                    use: ['style-loader', 'css-loader', 'stylus-loader'],
                    include: [
                        helpers.root('src', 'styles')
                    ]
                }
            ]
        },
        output: {
            path: helpers.root('dist'),
            //publicPath: '/',
            filename: '[name].bundle.js',
            sourceMapFilename: '[file].map'
            //,
            //chunkFileName: '[id].chunk.js'
        }
    }
);