const webpack = require('webpack');
const commonConfig = require("./webpack.common");
const webpackMerge = require("webpack-merge");
const helpers = require("./helpers");
const ENV = process.env.ENV = process.env.NODE_ENV = "production";

const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = webpackMerge(
    commonConfig({
        env: ENV
    }),
    {
        module: {
            rules: [
                {
                    test: /\.css$/,
                    loader: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: 'css-loader'
                    }),
                    include: [
                        helpers.root('src', 'styles')
                    ]
                },
                {
                    test: /\.styl$/,
                    loader: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: 'css-loader!stylus-loader'
                    }),
                    include: [
                        helpers.root('src', 'styles')
                    ]
                }
            ]
        },
        output: {
            path: helpers.root('dist'),
            publicPath: './',
            filename: '[name].[chunkhash].bundle.js',
            sourceMapFilename: '[file].map',
            chunkFilename: '[name].[chunkhash].chunk.js'
        },
        plugins: [
            new webpack.NoEmitOnErrorsPlugin(),

            new ExtractTextPlugin('[name].[hash].css')
        ]
    }
);